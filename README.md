# ANSiSHOW
*Scroll some ANSI art like it's 1998*

## About
This is an art project meant to imitate the experience you might get signing in to a BBS back in the 90s. Inspired by
an art exhibit from 2008 in San Francisco's "20 goto 10" gallery, this program uses pygame to take PNG images and scroll
them pleasantly on a modern display. 

[Video of the original exhibit](https://www.youtube.com/watch?v=r_cYOi3pnhA)

## Demo
Running on a Raspberry Pi Zero W in a framebuffer

![](./ansishow.mp4)

## Notes
This project doesn't use real ANSI/ASCII files since it's very difficult to make these render properly at the correct scale
on a modern widescreen LCD monitor. PNGs taken from https://16colo.rs are much easier to display and also look great.



